# Advent of Code 2023

## Days Completed

1. Trebuche?!
  - Rust: \*
2. Cube Conundrum
  - Rust: \*
3. Gear Ratios
  - Rust: unfinished
4. Scratchcards
  - Rust: \*
5. If You Give a Seed a Fertilizer
  - Rust: \*
6. Wait For it
  - Rust: \*\*
7. Camel Cards
  - Rust: unfinished
8. Haunted Wasteland
  - Rust: \*
9. Mirage Maintenance
  - Rust: \*
10. Pipe Maze
  - Rust: unfinished
11. Cosmic Expansion
  - Rust: \*\*

## License

The code for this repo is licensed under the BSD-3-Clause license
