use crate::convert_map;

pub fn process(input: &str) -> i32 {
    let map = convert_map(input);

    for l in map.iter() {
        println!("{l:?}");
    }

    todo!("day 10 - part 1");
}
