use std::usize;

use graph_builder::prelude::*;

pub mod part1;
pub mod part2;

type Map = Vec<Vec<char>>;
type MapDists = Vec<Vec<i32>>;
type Point = (usize, usize);


static mut VISITED: Vec<Point> = vec![];

pub fn convert_map(input: &str) -> Map {
    input.lines().map(|l| l.chars().collect()).collect()
}

fn create_dist_map((col, row): Point) -> MapDists {
    let mut result = vec![];
    for _ in 0..row {
        let mut temp = vec![];
        for _ in 0..col {
            temp.push(-1);
        }
        result.push(temp);
    }
    result
}

fn build_graph(map: Map) -> UndirectedCsrGraph<Point> {
    todo!()
}

pub fn walk(
    mut map: Map,
    mut dists: MapDists,
    (col, row): Point,
    dist: i32,
) -> Result<(Map, MapDists), Box<dyn std::error::Error>> {
    if col > map.len() || row > map[0].len() {
        return Err("Point cannot be outside map".into());
    }
    if map[row][col] == '.' {
        return Ok((map, dists));
    }

    if unsafe { VISITED.contains(&(col, row)) } {
        return Ok((map, dists));
    }

    if dists[row][col] != -1 {
        return Ok((map, dists));
    } else {
        dists[row][col] = dist;
    }

    // if dist > 3 {
    //     return Ok((map, dists));
    // }

    eprintln!(
        "point: ({col}, {row})\t val: {}\tdist: {dist}",
        map[row][col]
    );

    if row > 0 && matches!(map[row - 1][col], '7' | 'F' | '|') {
        eprintln!("There is a connection to the NORTH");
        (map, dists) = walk(map, dists, (col, row - 1), dist + 1)?;
    }
    if matches!(map[row + 1][col], 'J' | 'L' | '|') {
        eprintln!("There is a connection to the SOUTH");
        (map, dists) = walk(map, dists, (col, row + 1), dist + 1)?;
    }
    if col > 0 && matches!(map[row][col - 1], 'F' | 'L' | '-') {
        eprintln!("There is a connection to the WEST");
        (map, dists) = walk(map, dists, (col - 1, row), dist + 1)?;
    }
    if matches!(map[row][col + 1], 'J' | '7' | '-') {
        eprintln!("There is a connection to the EAST");
        (map, dists) = walk(map, dists, (col + 1, row), dist + 1)?;
    }

    Ok((map, dists))
}

pub fn find_start(map: &Map) -> Option<Point> {
    for (r, row) in map.iter().enumerate() {
        for (c, col) in row.iter().enumerate() {
            if *col == 'S' {
                return Some((c, r));
            }
        }
    }
    None
}

#[cfg(test)]
mod lib_tests {
    use crate::{convert_map, create_dist_map, find_start, walk};

    #[test]
    fn test_convert_map() {
        let input = ".....
.S-7.
.|.|.
.L-J.
.....";
        let result = convert_map(input);
        assert_eq!(
            result,
            vec![
                vec!['.', '.', '.', '.', '.'],
                vec!['.', 'S', '-', '7', '.'],
                vec!['.', '|', '.', '|', '.'],
                vec!['.', 'L', '-', 'J', '.'],
                vec!['.', '.', '.', '.', '.'],
            ]
        )
    }
    #[test]
    fn test_find_start() {
        let input = vec![
            vec!['.', '.', '.', '.', '.'],
            vec!['.', 'F', 'S', '7', '.'],
            vec!['.', '|', '.', '|', '.'],
            vec!['.', 'L', '-', 'J', '.'],
            vec!['.', '.', '.', '.', '.'],
        ];
        let start = find_start(&input).unwrap();
        assert_eq!(start, (2, 1));
    }
    #[test]
    fn test_walk() {
        let input = vec![
            vec!['.', '.', '.', '.', '.'],
            vec!['.', 'S', '-', '7', '.'],
            vec!['.', '|', '.', '|', '.'],
            vec!['.', 'L', '-', 'J', '.'],
            vec!['.', '.', '.', '.', '.'],
        ];
        let start = find_start(&input).unwrap();
        eprintln!("start: {start:?}");
        let (result, dists) = walk(
            input.clone(),
            create_dist_map((input[0].clone().len(), input.clone().len())),
            start,
            0,
        )
        .unwrap();

        for row in result.iter() {
            eprintln!("{row:?}");
        }
        for row in dists.iter() {
            eprintln!("{row:?}");
        }
        // eprintln!("{result:?}");
        assert!(false);
    }
}
