use rayon::prelude::*;

const MAX_RED: i32 = 12;
const MAX_GREEN: i32 = 13;
const MAX_BLUE: i32 = 14;

/// Returns number of cubes (RED, GREEN, BLUE)
fn parse_round(input: &str) -> (i32, i32, i32) {
    input.split(",").fold((0, 0, 0), |acc, b: &str| {
        let mut red = 0;
        let mut green = 0;
        let mut blue = 0;

        let parts = b.par_split_whitespace().collect::<Vec<&str>>();
        match parts[1] {
            "red" => red += parts[0].parse::<i32>().unwrap(),
            "green" => green += parts[0].parse::<i32>().unwrap(),
            "blue" => blue += parts[0].parse::<i32>().unwrap(),
            c => unreachable!("{c} not red, green, or blue"),
        }
        (acc.0 + red, acc.1 + green, acc.2 + blue)
    })
}

fn parse_game(input: &str) -> Option<i32> {
    let parts = input.split(":").collect::<Vec<&str>>();
    let id = parts[0].split_whitespace().collect::<Vec<&str>>()[1]
        .parse::<i32>()
        .unwrap();

    let invalid_rounds = parts[1]
        .par_split(';')
        .filter(|l| match parse_round(l) {
            (r, g, b) if r > MAX_RED || g > MAX_GREEN || b > MAX_BLUE => true,
            _ => false,
        })
        .count();

    if invalid_rounds > 0 {
        None
    } else {
        Some(id)
    }
}

fn proccess(input: &str) -> i32 {
    input
        .par_lines()
        .filter_map(parse_game)
        .fold(|| 0, |acc, id| acc + id)
        .sum()
}

fn main() {
    let input = include_str!("input.txt");
    let result = proccess(input);
    println!("{result}");
}

#[cfg(test)]
mod proc_test {
    use super::*;

    #[test]
    fn test_1() {
        let input = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";

        let result = proccess(input);
        assert_eq!(result, 8);
    }

    #[test]
    fn test_parse_game() {
        let tests = vec![
            (
                "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green",
                Some(1),
            ),
            (
                "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
                None,
            ),
        ];

        for (input, expected) in tests {
            let output = parse_game(input);
            if output != expected {
                eprintln!("Incorrect output. got={output:?}, expected={expected:?}");
                assert_eq!(output, expected);
            }
        }
    }

    #[test]
    fn test_parse_rount() {
        let tests = vec![
            ("3 blue, 4 red", (4, 0, 3)),
            ("1 red, 2 green, 6 blue", (1, 2, 6)),
        ];
        for (input, expected) in tests {
            let output = parse_round(input);
            if output != expected {
                eprintln!("Incorrect output. got={output:?}, expected={expected:?}");
                assert_eq!(output, expected);
            }
        }
    }
}
