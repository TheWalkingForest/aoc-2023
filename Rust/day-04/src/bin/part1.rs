use day_4::part1::proccess;


fn main() {
    let input = include_str!("input.txt");
    let result = proccess(input);

    println!("{result}");
}
