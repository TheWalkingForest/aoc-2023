pub mod part1 {
    use std::collections::HashSet;

    #[cfg(feature = "par")]
    use rayon::prelude::*;

    #[cfg(not(feature = "par"))]
    fn parse_card(input: &str) -> i32 {
        let parts = input.split(':').collect::<Vec<&str>>();
        let plays = parts[1].split('|').collect::<Vec<&str>>()[0]
            .split_whitespace()
            .map(|n| n.parse::<i32>().unwrap())
            .collect::<HashSet<i32>>();
        let draw = parts[1].split('|').collect::<Vec<&str>>()[1]
            .split_whitespace()
            .map(|n| n.parse::<i32>().unwrap())
            .collect::<HashSet<i32>>();

        let overlap = plays.intersection(&draw).count();

        if overlap > 0 {
            0b1 << (overlap - 1)
        } else {
            overlap as i32
        }
    }

    #[cfg(feature = "par")]
    fn parse_card(input: &str) -> i32 {
        let parts = input.split(':').collect::<Vec<&str>>();
        let plays = parts[1].split('|').collect::<Vec<&str>>()[0]
            .par_split_whitespace()
            .map(|n| n.parse::<i32>().unwrap())
            .collect::<HashSet<i32>>();
        let draw = parts[1].split('|').collect::<Vec<&str>>()[1]
            .par_split_whitespace()
            .map(|n| n.parse::<i32>().unwrap())
            .collect::<HashSet<i32>>();

        let overlap = plays.intersection(&draw).count();

        if overlap > 0 {
            0b1 << (overlap - 1)
        } else {
            overlap as i32
        }
    }

    pub fn proccess(input: &str) -> i32 {
        #[cfg(feature = "par")]
        return input.par_lines().map(parse_card).sum();

        #[cfg(not(feature = "par"))]
        return input.lines().map(parse_card).sum();
    }
    #[cfg(test)]
    mod proc_tests {
        use super::*;

        #[test]
        fn test_1() {
            let input = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";

            let result = proccess(input);
            assert_eq!(result, 13);
        }

        #[test]
        fn test_parse_card() {
            let tests = vec![
                ("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53", 8),
                ("Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1", 2),
                ("Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36", 0),
            ];
            for (i, (input, expected)) in tests.iter().enumerate() {
                let output = parse_card(input);
                if output != *expected {
                    eprintln!("[{i}] Incorrect output. got={output:?}, expected={expected:?}");
                    assert_eq!(output, *expected);
                }
            }
        }
    }
}
