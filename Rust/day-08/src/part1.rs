use crate::Map;

pub fn process(input: &str) -> i32 {
    Map::parse_map(input).walk()
}

#[cfg(test)]
mod proc_test {

    use super::process;

    #[test]
    fn test_1() {
        let input = "RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)";
        let result = process(input);
        assert_eq!(result, 2);
    }
}
