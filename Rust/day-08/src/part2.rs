use crate::Map;

pub fn process(input: &str) -> i32 {
    let map = Map::parse_map(input);
    let start_nodes = map.get_start_nodes();

    map.par_walk(start_nodes)
}

#[cfg(test)]
mod proc_test {

    use super::process;

    #[test]
    // #[ignore = "reason"]
    fn test_1() {
        let input = "LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)";

        let result = process(input);
        assert_eq!(result, 6);
    }
}
