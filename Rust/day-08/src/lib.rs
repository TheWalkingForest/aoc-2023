use std::collections::HashMap;

use itertools::izip;
use rayon::prelude::*;
use regex::Regex;

pub mod part1;
pub mod part2;

type Instructions = String;

#[derive(Debug, PartialEq, Clone)]
pub struct Node {
    name: String,
    left: String,
    right: String,
}

#[derive(Debug, PartialEq)]
pub struct Map {
    instructions: Instructions,
    nodes: HashMap<String, Node>,
}

impl Map {
    pub fn walk(&self) -> i32 {
        let mut steps = 0;
        let mut curr_node = self
            .nodes
            .get("AAA".into())
            .expect("Node AAA Does not exist");
        let mut instructions = self.instructions.chars().cycle();

        let mut i;
        while curr_node.name != "ZZZ" {
            i = instructions.next().unwrap();
            match i {
                'L' => {
                    curr_node = self
                        .nodes
                        .get(&curr_node.left)
                        .expect(format!("Node {} does not exist", curr_node.left).as_str());
                }
                'R' => {
                    curr_node = self
                        .nodes
                        .get(&curr_node.right)
                        .expect(format!("Node {} does not exist", curr_node.right).as_str());
                }
                _ => unreachable!(),
            }
            steps += 1;
        }

        steps
    }

    pub fn par_walk(&self, start_nodes: Vec<String>) -> i32 {
        let mut steps = 0;

        let mut curr_nodes = start_nodes
            .par_iter()
            .map(|name| {
                self.nodes
                    .get(name)
                    .expect(format!("Node {} does not exist", name).as_str())
            })
            .collect::<Vec<_>>();

        let mut instructions = self.instructions.chars().cycle();

        let mut i;
        while curr_nodes.par_iter().filter(|n| n.name.ends_with("Z")).count() != start_nodes.len() {
            i = instructions.next().unwrap();
            eprint!("{i}\t");
            eprintln!("curr_nodes: {curr_nodes:?}");
            for n in curr_nodes.iter_mut() {
                eprintln!("{n:?}");
                match i {
                    'L' => {
                        *n = self
                            .nodes
                            .get(&n.left)
                            .expect(format!("Node {} does not exist", n.left).as_str());
                    }
                    'R' => {
                        *n = self
                            .nodes
                            .get(&n.right)
                            .expect(format!("Node {} does not exist", n.right).as_str());
                    }
                    _ => unreachable!(),
                }
            }
            std::thread::sleep(std::time::Duration::from_millis(50));
            steps += 1;
        }

        steps
    }

    pub fn parse_map(input: &str) -> Map {
        let mut lines = input.lines();
        let instructions = lines.next().unwrap().to_string();
        lines.next();

        let re = Regex::new(r"[A-Z]{2}[A-Z]{1}").expect("Invalid regex");

        let mut nodes = HashMap::new();
        for l in lines {
            let matches = re.find_iter(l).map(|m| m.as_str()).collect::<Vec<_>>();
            nodes.insert(
                matches[0].into(),
                Node {
                    name: matches[0].into(),
                    left: matches[1].into(),
                    right: matches[2].into(),
                },
            );
        }

        Map {
            instructions,
            nodes,
        }
    }

    pub fn get_start_nodes(&self) -> Vec<String> {
        let mut result = Vec::new();
        for (name, _) in self.nodes.iter() {
            if name.ends_with("A") {
                result.push(name.clone())
            }
        }
        println!("start nodes: {result:?}");
        result
    }
}

#[cfg(test)]
mod lib_tests {
    use std::collections::HashMap;

    use crate::{Map, Node};

    #[test]
    fn test_parse_map() {
        let input = "LLR

AAA = (BBB, CCC)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)";
        let result = Map::parse_map(input);
        assert_eq!(
            result,
            Map {
                instructions: "LLR".to_string(),
                nodes: HashMap::from([
                    (
                        "AAA".into(),
                        Node {
                            name: "AAA".into(),
                            left: "BBB".into(),
                            right: "CCC".into(),
                        }
                    ),
                    (
                        "BBB".into(),
                        Node {
                            name: "BBB".into(),
                            left: "AAA".into(),
                            right: "ZZZ".into(),
                        }
                    ),
                    (
                        "ZZZ".into(),
                        Node {
                            name: "ZZZ".into(),
                            left: "ZZZ".into(),
                            right: "ZZZ".into(),
                        }
                    ),
                ])
            }
        )
    }

    #[test]
    fn test_walk() {
        let input = Map {
            instructions: "LLR".to_string(),
            nodes: HashMap::from([
                (
                    "AAA".into(),
                    Node {
                        name: "AAA".into(),
                        left: "BBB".into(),
                        right: "BBB".into(),
                    },
                ),
                (
                    "BBB".into(),
                    Node {
                        name: "BBB".into(),
                        left: "AAA".into(),
                        right: "ZZZ".into(),
                    },
                ),
                (
                    "ZZZ".into(),
                    Node {
                        name: "ZZZ".into(),
                        left: "ZZZ".into(),
                        right: "ZZZ".into(),
                    },
                ),
            ]),
        };
        let result = input.walk();
        assert_eq!(result, 6)
    }

    #[test]
    fn test_get_start_nodes() {
        let input = "LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)";
        let result = Map::parse_map(input).get_start_nodes().sort();
        assert_eq!(result, vec!["11A", "22A"].sort());
    }
}
