use std::collections::BTreeMap;

use crate::{get_empty_cols, get_empty_rows};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Galaxy {
    pub row: usize,
    pub col: usize,
}

fn parse_galaxy(input: &str) -> BTreeMap<usize, Galaxy> {
    let mut result = BTreeMap::new();
    let mut count = 0;
    for (r, row) in input.lines().enumerate() {
        for (c, col) in row.chars().enumerate() {
            if col == '#' {
                result.insert(count, Galaxy { row: r, col: c });
                count += 1;
            }
        }
    }
    result
}

fn expand_galaxy(
    mut galaxy_set: BTreeMap<usize, Galaxy>,
    input: Vec<Vec<char>>,
    num_inserts: usize,
) -> BTreeMap<usize, Galaxy> {
    let empty_rows = get_empty_rows(&input);
    // println!("empty_rows: {empty_rows:?}");
    let empty_cols = get_empty_cols(&input);
    // println!("empty_cols: {empty_cols:?}");

    for (_, g) in galaxy_set.iter_mut() {
        let empty_row_up = empty_rows.iter().filter(|&&e| e < g.row).count();
        // println!("{g:?} has {empty_row_up} empty rows above");
        let empty_col_left = empty_cols.iter().filter(|&&e| e < g.col).count();
        // println!("{g:?} has {empty_col_left} empty cols to the left");

        // g.row = 4
        // g.col = 6
        // 2 cols left
        // 1 row above
        // num insert = 2

        //         (4 - 1) + (1 * 2)
        //         3 + 2
        //         5
        g.row = (g.row - empty_row_up) + (empty_row_up * num_inserts);

        // (6 - 2) + (2 * 2)
        // 4 + 4
        // 8
        g.col = (g.col - empty_col_left) + (empty_col_left * num_inserts);
    }
    galaxy_set
}

pub fn find_distances(map: &BTreeMap<usize, Galaxy>) -> BTreeMap<(usize, usize), usize> {
    // println!("=> Finding distances");
    let mut result = BTreeMap::new();

    for (start, g) in map.iter() {
        for end in 0..map.len() {
            if *start == end {
                continue;
            }
            if result.contains_key(&(end.clone(), start.clone())) {
                continue;
            }
            let Galaxy { row: y2, col: x2 } = map.get(&end).unwrap();
            let dist = usize::max(g.col, *x2) - usize::min(g.col, *x2) + usize::max(g.row, *y2)
                - usize::min(g.row, *y2);
            result.insert((*start, end), dist);
        }
    }
    result
}

pub fn process(input: &str) -> usize {
    let galaxy = parse_galaxy(input);
    // println!("{galaxy:?}");
    let galaxy = expand_galaxy(galaxy, crate::parse_galaxy(input), 1_000_000);
    // println!("{galaxy:?}");
    let dists = find_distances(&galaxy);
    dists.values().sum()
}

#[cfg(test)]
mod part2_tests {
    use crate::part2::process;

    #[test]
    fn test_part2() {
        let input = "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....";
        let result = process(input);
        assert_eq!(result, 8410);
    }
}
