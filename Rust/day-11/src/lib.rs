use std::collections::BTreeMap;

pub mod part1;
pub mod part2;

type Galaxy = Vec<Vec<char>>;

fn galaxy_to_string(input: &Galaxy) -> String {
    let result = input
        .iter()
        .map(|l| l.iter().collect::<String>())
        .collect::<Vec<String>>()
        .join("\n");

    result
}

fn parse_galaxy(input: &str) -> Galaxy {
    input.lines().map(|l| l.chars().collect()).collect()
}

fn get_empty_rows(input: &Galaxy) -> Vec<usize> {
    let mut empty_rows = Vec::new();
    for (n, l) in input.iter().enumerate() {
        if l.iter().all(|&c| c == '.') {
            empty_rows.push(n);
        }
    }
    empty_rows
}

fn get_empty_cols(input: &Galaxy) -> Vec<usize> {
    let mut empty_cols = Vec::new();

    let width = input[0].len();
    let height = input.len();

    let mut w = 0;
    while w < width {
        let mut h = 0;
        while h < height {
            if input[h][w] != '.' {
                break;
            }
            h += 1;
        }
        if h == height {
            empty_cols.push(w);
        }
        w += 1;
    }
    empty_cols
}

fn expand_galaxy(mut input: Galaxy, num_inserts: usize) -> Galaxy {
    println!("=> expanding galaxy");
    let empty_rows = get_empty_rows(&input);
    let empty_cols = get_empty_cols(&input);


    println!("=> adding rows");
    for row in empty_rows.iter().rev() {
        for i in 0..num_inserts {
            input.insert(*row, vec!['.'; input[0].len()]);
        }
    }

    println!("=> adding cols");
    for col in empty_cols.iter().rev() {
        for r in input.iter_mut() {
            for i in 0..num_inserts {
                r.insert(*col, '.');
            }
        }
    }

    input
}

pub fn number_galaxies(input: &Galaxy) -> BTreeMap<usize, (usize, usize)> {
    println!("=> numbering galaxies");
    let mut count = 0;

    let mut map = BTreeMap::new();

    for (r, row) in input.iter().enumerate() {
        for (c, col) in row.iter().enumerate() {
            if *col == '#' {
                map.insert(count, (c, r));
                count += 1;
            }
        }
    }
    map
}

pub fn find_distances(map: &BTreeMap<usize, (usize, usize)>) -> BTreeMap<(usize, usize), usize> {
    println!("=> Finding distances");
    let mut result = BTreeMap::new();

    for (start, (x1, y1)) in map.iter() {
        for end in 0..map.len() {
            if *start == end {
                continue;
            }
            if result.contains_key(&(end.clone(), start.clone())) {
                continue;
            }
            let (x2, y2) = map.get(&end).unwrap();
            let dist = usize::max(*x1, *x2) - usize::min(*x1, *x2) + usize::max(*y1, *y2)
                - usize::min(*y1, *y2);
            result.insert((*start, end), dist);
        }
    }
    result
}

#[cfg(test)]
mod lib_tests {
    use std::collections::{BTreeMap, HashMap};

    use crate::{
        expand_galaxy, galaxy_to_string, get_empty_cols, get_empty_rows, number_galaxies,
        parse_galaxy,
    };

    #[test]
    fn test_get_empty_rows() {
        let input = "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....";
        let input = parse_galaxy(input);
        let result = get_empty_rows(&input);

        assert_eq!(result, vec![3, 7]);
    }

    #[test]
    fn test_get_empty_cols() {
        let input = "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....";
        let input = parse_galaxy(input);
        let result = get_empty_cols(&input);
        assert_eq!(result, vec![2, 5, 8]);
    }

    #[test]
    fn test_expand_galaxy() {
        let input = "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....";
        let input = parse_galaxy(input);
        let result = expand_galaxy(input, 1);

        eprintln!("{}", galaxy_to_string(&result));
        eprintln!("==========");
        eprintln!(
            "....#........
.........#...
#............
.............
.............
........#....
.#...........
............#
.............
.............
.........#...
#....#......."
        );

        assert_eq!(
            galaxy_to_string(&result),
            "....#........
.........#...
#............
.............
.............
........#....
.#...........
............#
.............
.............
.........#...
#....#......."
        );
    }
    #[test]
    fn test_galaxy_to_string() {
        let input = vec![vec!['.', '.'], vec!['.', '.']];
        let result = galaxy_to_string(&input);
        assert_eq!(result, "..\n..");
    }

    #[test]
    fn test_number_galaxies() {
        let input = vec![vec!['#', '.'], vec!['#', '#']];
        let map = number_galaxies(&input);
        let mut expected = BTreeMap::new();
        expected.insert(0, (0, 0));
        expected.insert(1, (0, 1));
        expected.insert(2, (1, 1));

        assert_eq!(map, expected);
    }
}
