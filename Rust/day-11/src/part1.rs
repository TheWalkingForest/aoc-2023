use crate::{expand_galaxy, find_distances, number_galaxies, parse_galaxy};

pub fn process(input: &str) -> usize {
    let galaxy = parse_galaxy(input);
    let galaxy = expand_galaxy(galaxy, 1);
    let map = number_galaxies(&galaxy);
    let dists = find_distances(&map);

    dists.values().sum()
}

#[cfg(test)]
mod part1_tests {
    use std::collections::BTreeMap;

    use crate::{number_galaxies, part1::find_distances};

    use super::process;

    #[test]
    fn test_part1() {
        let input = "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....";
        let result = process(input);
        assert_eq!(result, 374);
    }

    #[test]
    fn test_find_distances() {
        let input = vec![vec!['#', '.'], vec!['#', '#']];

        let map = number_galaxies(&input);
        let dists = find_distances(&map);

        let mut expected = BTreeMap::new();
        expected.insert((0, 1), 1);
        expected.insert((0, 2), 2);
        expected.insert((1, 2), 1);

        assert_eq!(dists, expected);
    }
}
