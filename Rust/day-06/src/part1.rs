use itertools::zip_eq;

use crate::{play_game, Race};

fn parse_game(input: &str) -> Vec<Race> {
    let lines = input.lines().collect::<Vec<&str>>();
    let times = lines[0]
        .split_whitespace()
        .skip(1)
        .map(|e| e.parse::<usize>().unwrap());
    let dists = lines[1]
        .split_whitespace()
        .skip(1)
        .map(|e| e.parse::<usize>().unwrap());
    let mut result = Vec::new();
    for (time, best_dist) in zip_eq(times, dists) {
        result.push(Race { time, best_dist })
    }
    result
}

pub fn process(input: &str) -> usize {
    parse_game(input)
        .iter()
        .map(play_game)
        .reduce(|acc, w| acc * w)
        .unwrap()
}

#[cfg(test)]
mod proc_test {
    use crate::{
        part1::{parse_game, process},
        play_game, Race,
    };

    #[test]
    fn test_1() {
        let input = "Time:      7  15   30
Distance:  9  40  200";
        let ressult = process(input);
        assert_eq!(ressult, 288)
    }

    #[test]
    fn test_parse_race() {
        let input = "Time:      7
Distance:  9";
        let result = parse_game(input);
        assert_eq!(
            result[0],
            Race {
                time: 7,
                best_dist: 9
            }
        );
    }

    #[test]
    fn test_play_game() {
        let input = Race {
            time: 7,
            best_dist: 9,
        };
        let result = play_game(&input);
        assert_eq!(result, 4);
    }
}
