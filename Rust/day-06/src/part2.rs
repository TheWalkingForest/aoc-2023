use itertools::Itertools;

use crate::{play_game, Race};

fn parse_game(input: &str) -> Race {
    let lines = input.lines().collect::<Vec<&str>>();
    let time = lines[0]
        .split_whitespace()
        .skip(1)
        .join("")
        .parse::<usize>()
        .unwrap();

    let best_dist = lines[1]
        .split_whitespace()
        .skip(1)
        .join("")
        .parse::<usize>()
        .unwrap();

    Race { time, best_dist }
}

pub fn process(input: &str) -> usize {
    play_game(&parse_game(input))
}

#[cfg(test)]
mod proc_test {
    use crate::{play_game, Race};

    use super::*;

    #[test]
    #[ignore]
    fn test_1() {
        let input = "Time:      7  15   30
Distance:  9  40  200";
        let ressult = process(input);
        assert_eq!(ressult, 288)
    }

    #[test]
    fn test_parse_race() {
        let input = "Time:      7 15  30
Distance:  9 40 200";
        let result = parse_game(input);
        assert_eq!(
            result,
            Race {
                time: 71530,
                best_dist: 940200
            }
        );
    }

    #[test]
    fn test_play_game() {
        let input = Race {
            time: 7,
            best_dist: 9,
        };
        let result = play_game(&input);
        assert_eq!(result, 4);
    }
}

