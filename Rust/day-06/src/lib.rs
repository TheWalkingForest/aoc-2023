pub mod part1;
pub mod part2;

#[derive(Debug, PartialEq)]
struct Race {
    time:      usize,
    best_dist: usize,
}

fn play_game(Race { time, best_dist }: &Race) -> usize {
    let mut num_wins = 0;
    for hold_button in 0..*time {
        let time_for_travel = *time - hold_button;
        let distance_traveled = time_for_travel * hold_button;
        if distance_traveled > *best_dist {
            num_wins += 1;
        }
    }
    num_wins
}
