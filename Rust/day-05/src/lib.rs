pub mod part1;
pub mod part2;

#[allow(dead_code)]
#[derive(Debug)]
pub struct Map {
    destination: usize,
    source: usize,
    length: usize,
}

impl Map {
    pub fn new(destination: usize, source: usize, length: usize) -> Self {
        Self {
            destination,
            source,
            length,
        }
    }
}

#[allow(dead_code)]
#[derive(Debug, Default)]
pub struct Almanac {
    seeds: Vec<usize>,
    seed_to_soil: Vec<Map>,
    soil_to_fert: Vec<Map>,
    fert_to_water: Vec<Map>,
    water_to_light: Vec<Map>,
    ligth_to_temp: Vec<Map>,
    temp_to_humid: Vec<Map>,
    humid_to_loc: Vec<Map>,
}

impl Almanac {
    pub fn parse_almanac(input: &str) -> Option<Self> {
        let mut seed_to_soil = Vec::new();
        let mut soil_to_fert = Vec::new();
        let mut fert_to_water = Vec::new();
        let mut water_to_light = Vec::new();
        let mut ligth_to_temp = Vec::new();
        let mut temp_to_humid = Vec::new();
        let mut humid_to_loc = Vec::new();

        let mut lines = input.lines();
        let Some(seed_line) = lines.next() else {
            return None;
        };
        let seeds = seed_line
            .split(':')
            .skip(1)
            .collect::<String>()
            .split_whitespace()
            .map(|e| e.parse().unwrap())
            .collect::<Vec<usize>>();

        lines.next();
        lines.next();
        while let Some(line) = lines.next() {
            if line.is_empty() {
                break;
            }
            let l = line
                .split_whitespace()
                .map(|e| e.parse::<usize>().unwrap())
                .collect::<Vec<usize>>();
            seed_to_soil.push(Map::new(l[0], l[1], l[2]));
        }

        lines.next();
        while let Some(line) = lines.next() {
            if line.is_empty() {
                break;
            }
            let l = line
                .split_whitespace()
                .map(|e| e.parse::<usize>().unwrap())
                .collect::<Vec<usize>>();
            soil_to_fert.push(Map::new(l[0], l[1], l[2]));
        }

        lines.next();
        while let Some(line) = lines.next() {
            if line.is_empty() {
                break;
            }
            let l = line
                .split_whitespace()
                .map(|e| e.parse::<usize>().unwrap())
                .collect::<Vec<usize>>();
            fert_to_water.push(Map::new(l[0], l[1], l[2]));
        }

        lines.next();
        while let Some(line) = lines.next() {
            if line.is_empty() {
                break;
            }
            let l = line
                .split_whitespace()
                .map(|e| e.parse::<usize>().unwrap())
                .collect::<Vec<usize>>();
            water_to_light.push(Map::new(l[0], l[1], l[2]));
        }
        lines.next();
        while let Some(line) = lines.next() {
            if line.is_empty() {
                break;
            }
            let l = line
                .split_whitespace()
                .map(|e| e.parse::<usize>().unwrap())
                .collect::<Vec<usize>>();
            ligth_to_temp.push(Map::new(l[0], l[1], l[2]));
        }
        lines.next();
        while let Some(line) = lines.next() {
            if line.is_empty() {
                break;
            }
            let l = line
                .split_whitespace()
                .map(|e| e.parse::<usize>().unwrap())
                .collect::<Vec<usize>>();
            temp_to_humid.push(Map::new(l[0], l[1], l[2]));
        }
        lines.next();
        while let Some(line) = lines.next() {
            if line.is_empty() {
                break;
            }
            let l = line
                .split_whitespace()
                .map(|e| e.parse::<usize>().unwrap())
                .collect::<Vec<usize>>();
            humid_to_loc.push(Map::new(l[0], l[1], l[2]));
        }

        Some(Self {
            seeds,
            seed_to_soil,
            soil_to_fert,
            fert_to_water,
            water_to_light,
            ligth_to_temp,
            temp_to_humid,
            humid_to_loc,
        })
    }

    pub fn seeds_to_loc(&self) -> usize {
        self.seeds
            .iter()
            .map(|seed| {
                for m in self.seed_to_soil.iter() {
                    if (m.source..(m.source + m.length)).contains(seed) {
                        return m.destination + (seed - m.source);
                    }
                }
                seed.clone()
            })
            .map(|soil| {
                for m in self.soil_to_fert.iter() {
                    if (m.source..(m.source + m.length)).contains(&soil) {
                        return m.destination + (soil - m.source);
                    }
                }
                soil.clone()
            })
            .map(|fert| {
                for m in self.fert_to_water.iter() {
                    if (m.source..(m.source + m.length)).contains(&fert) {
                        return m.destination + (fert - m.source);
                    }
                }
                fert.clone()
            })
            .map(|water| {
                for m in self.water_to_light.iter() {
                    if (m.source..(m.source + m.length)).contains(&water) {
                        return m.destination + (water - m.source);
                    }
                }
                water.clone()
            })
            .map(|light| {
                for m in self.ligth_to_temp.iter() {
                    if (m.source..(m.source + m.length)).contains(&light) {
                        return m.destination + (light - m.source);
                    }
                }
                light.clone()
            })
            .map(|temp| {
                for m in self.temp_to_humid.iter() {
                    if (m.source..(m.source + m.length)).contains(&temp) {
                        return m.destination + (temp - m.source);
                    }
                }
                temp.clone()
            })
            .map(|humid| {
                for m in self.humid_to_loc.iter() {
                    if (m.source..(m.source + m.length)).contains(&humid) {
                        return m.destination + (humid - m.source);
                    }
                }
                humid.clone()
            })
            .min()
            .unwrap()
    }

    pub fn part1_to_part2(self) -> Option<Self> {
        let mut seeds = self.seeds.clone();
        for i in self.seeds.as_slice().windows(2).step_by(2) {
            // println!("{}", i[0]);
            seeds.append(&mut ((i[0] + 1) .. (i[1] - 1)).collect::<Vec<usize>>())
        }
        if seeds.len() <= self.seeds.len() {
            panic!("failed to expand seeds")
        }
        Some(Self {
            seeds,
            ..self
        })
    }
}
