use crate::{get_diffs, parse_input};
use rayon::prelude::*;

fn process_line(input: Vec<i32>) -> i32 {
    let mut result = vec![input];

    while result.last().unwrap().iter().filter(|&&n| n == 0).count() != result.last().unwrap().len()
    {
        let t = get_diffs(result.last().unwrap().to_vec());
        result.push(t);
    }

    println!("result: {result:?}");

    let mut i = result.len() - 1;
    while i >= 1 {
        let t =
            result[i - 1].iter().take(1).next().unwrap() - result[i].iter().take(1).next().unwrap();
        println!("t: {t}");
        result[i] = [vec![t], result[i].clone()].concat();
        i -= 1;
    }
    eprintln!("{result:?}");
    *result[0].iter().take(1).next().unwrap()
}

pub fn process(input: &str) -> i32 {
    parse_input(input)
        .par_iter()
        .map(|l| process_line(l.to_vec()))
        .sum()
}

#[cfg(test)]
mod proc_test {
    use super::*;

    #[test]
    fn test_1() {
        let input = "0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45";
        let result = process(input);
        assert_eq!(result, 2)
    }

    #[test]
    fn test_process_line() {
        // let input = vec![0, 3, 6, 9, 12, 15];
        // let result = process_line(input);
        // assert_eq!(result, -3);

        let input = vec![10, 13, 16, 21, 30, 45];
        let result = process_line(input);
        assert_eq!(result, 5);
    }
}

