pub mod part1;
pub mod part2;

use rayon::prelude::*;

pub fn parse_input(input: &str) -> Vec<Vec<i32>> {
    input
        .par_lines()
        .map(|l| {
            l.par_split_whitespace()
                .map(|n| n.parse::<i32>().unwrap())
                .collect::<Vec<_>>()
        })
        .collect::<Vec<Vec<_>>>()
}

pub fn get_diffs(input: Vec<i32>) -> Vec<i32> {
    if input.par_iter().filter(|&&n| n == 0).count() == input.len() {
        return input;
    }

    let mut diffs = Vec::new();

    for win in input.windows(2) {
        diffs.push(win[1] - win[0]);
    }

    diffs
}

#[cfg(test)]
mod lib_tests {
    use super::*;

    #[test]
    fn test_parse() {
        let input = "0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45";
        let result = parse_input(input);
        assert_eq!(
            result,
            vec![
                vec![0, 3, 6, 9, 12, 15],
                vec![1, 3, 6, 10, 15, 21],
                vec![10, 13, 16, 21, 30, 45],
            ]
        )
    }

    #[test]
    fn test_get_diffs() {
        let input = vec![10, 13, 16, 21, 30, 45];
        let result = get_diffs(input);
        assert_eq!(result, vec![3, 3, 5, 9, 15]);

        let result = get_diffs(result);
        assert_eq!(result, vec![0, 2, 4, 6]);

        let result = get_diffs(result);
        assert_eq!(result, vec![2, 2, 2]);

        let result = get_diffs(result);
        assert_eq!(result, vec![0, 0]);

        let result = get_diffs(result);
        assert_eq!(result, vec![0, 0]);
    }
}
