use day_7::part1::process;

fn main() {
    let input = include_str!("input.txt");
    let result = process(input);

    println!("{result}");
}
