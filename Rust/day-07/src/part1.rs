use std::{cmp::Ordering, collections::HashMap};

use itertools::zip_eq;

#[derive(Debug, PartialEq, PartialOrd, Eq, Ord, Hash, Clone, Copy)]
enum Card {
    A = 14,
    K = 13,
    Q = 12,
    J = 11,
    T = 10,
    _9 = 9,
    _8 = 8,
    _7 = 7,
    _6 = 6,
    _5 = 5,
    _4 = 4,
    _3 = 3,
    _2 = 2,
}

impl Into<Card> for char {
    fn into(self) -> Card {
        match self {
            'A' => Card::A,
            'K' => Card::K,
            'Q' => Card::Q,
            'J' => Card::J,
            'T' => Card::T,
            '9' => Card::_9,
            '8' => Card::_8,
            '7' => Card::_7,
            '6' => Card::_6,
            '5' => Card::_4,
            '4' => Card::_3,
            '3' => Card::_3,
            '2' => Card::_2,
            _ => unreachable!(),
        }
    }
}

fn char_to_card(input: char) -> Card {
    input.into()
}

#[derive(Debug, PartialEq, PartialOrd, Eq, Ord)]
enum Hand {
    Five = 7,
    Four = 6,
    FullHouse = 5,
    Three = 4,
    TwoPair = 3,
    Pair = 2,
    HighCard = 1,
}

fn parse_hand(input: &str) -> (&str, i32) {
    let p = input.split_whitespace().collect::<Vec<&str>>();
    let bid = p[1].parse::<i32>().unwrap();
    (p[0], bid)
}

fn get_hand((cards, bid): (&str, i32)) -> (Hand, &str, i32) {
    let mut card_count: HashMap<char, i32> = HashMap::new();

    for c in cards.chars() {
        card_count.insert(c, *(card_count.get(&c).unwrap_or(&0)) + 1);
    }

    let hand = match card_count.len() {
        1 => Hand::Five,
        2 => {
            let (_, count) = card_count.iter().next().unwrap();
            match 5 - count {
                2 | 3 => Hand::FullHouse,
                1 | 4 => Hand::Four,
                _ => unreachable!("card with 2 groups"),
            }
        }
        3 => {
            let mut card_iter = card_count.iter();
            let (_, count1) = card_iter.next().unwrap();
            let (_, count2) = card_iter.next().unwrap();
            match (count1, count2) {
                (2, 2) | (2, 1) | (1, 2) => Hand::TwoPair,
                (1, 1) | (1, 3) | (3, 1) => Hand::Three,
                _ => unreachable!("card with 3 groups: {cards}"),
            }
        }
        4 => Hand::Pair,
        5 => Hand::HighCard,
        _ => unreachable!(),
    };

    (hand, cards, bid)
}

fn sort_hands(mut input: Vec<(Hand, &str, i32)>) -> Vec<(Hand, &str, i32)> {
    input.sort_by(|lhs, rhs| {
        if lhs.0 != rhs.0 {
            lhs.0.cmp(&rhs.0)
        } else {
            for (l, r) in zip_eq(lhs.1.chars(), rhs.1.chars()) {
                if char_to_card(l) > char_to_card(r) {
                    Ordering::Greater;
                } else if char_to_card(l) < char_to_card(r) {
                    return Ordering::Less;
                }
            }
            Ordering::Equal
        }
    });
    input
}

pub fn process(input: &str) -> i32 {
    let plays = input
        .lines()
        .map(parse_hand)
        .map(get_hand)
        .collect::<Vec<(Hand, &str, i32)>>();
    let (result, _) = sort_hands(plays)
        .iter()
        .fold((0, 1), |(acc, rank), (_, _, b)| {
            (acc + (rank * b), rank + 1)
        });
    result
}

#[cfg(test)]
mod proc_tests {
    use super::*;

    #[test]
    fn test_1() {
        let input = "32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";
        let result = process(input);
        assert_eq!(result, 6440)
    }

    #[test]
    fn test_parse_hand() {
        let input = "32T3K 765";
        let result = parse_hand(input);
        assert_eq!(result, ("32T3K", 765))
    }

    #[test]
    fn test_get_hand() {
        let tests = [
            (("32T3K", 0), (Hand::Pair, "", 0)),
            (("32T5K", 0), (Hand::HighCard, "", 0)),
            (("3233K", 0), (Hand::Three, "", 0)),
            (("32332", 0), (Hand::FullHouse, "", 0)),
            (("33323", 0), (Hand::Four, "", 0)),
            (("33333", 0), (Hand::Five, "", 0)),
            (("32T32", 0), (Hand::TwoPair, "", 0)),
        ];

        for (input, (hand, _, _)) in tests {
            let result = get_hand(input);
            if result.0 != hand {
                panic!("hand: {}. got={:?}, expected={:?}", input.0, result.0, hand);
            }
        }
    }
}
