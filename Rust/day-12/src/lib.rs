use std::usize;

pub mod part1;
pub mod part2;

#[derive(Debug, PartialEq, PartialOrd)]
struct Row {
    springs: String,
    groups: Vec<usize>,
}

fn parse_data(input: &str) -> Vec<Row> {
    input
        .lines()
        .map(|l| {
            let line = l.split_whitespace().collect::<Vec<&str>>();
            let springs = line[0].to_string();
            let groups = line[1]
                .split(',')
                .map(|e| e.parse::<usize>().unwrap())
                .collect::<Vec<usize>>();

            Row { springs, groups }
        })
        .collect()
}

fn find_arrangements(row: &Row) -> usize {
    // let s = row.springs.split('.').filter(|e| e.is_empty()).collect::<Vec<&str>>();

    let mut curr_item = row.groups[0];
    let mut seg_len = 1;
    let mut start = 0;

    let curr_seg = &row.springs[start..(start + seg_len)];
    dbg!(&curr_seg);

    if curr_seg.len() == curr_item {}

    todo!()
}

mod lib_tests {
    use super::*;

    #[test]
    fn test_parse_data() {
        let input = "???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1";
        let expected = vec![
            Row {
                springs: "???.###".into(),
                groups: vec![1, 1, 3],
            },
            Row {
                springs: ".??..??...?##.".into(),
                groups: vec![1, 1, 3],
            },
            Row {
                springs: "?#?#?#?#?#?#?#?".into(),
                groups: vec![1, 3, 1, 6],
            },
            Row {
                springs: "????.#...#...".into(),
                groups: vec![4, 1, 1],
            },
            Row {
                springs: "????.######..#####.".into(),
                groups: vec![1, 6, 5],
            },
            Row {
                springs: "?###????????".into(),
                groups: vec![3, 2, 1],
            },
        ];

        let result = parse_data(input);
        assert_eq!(result, expected)
    }

    #[test]
    fn test_find_arrangements() {
        let tests = vec![
            (
                Row {
                    springs: "???.###".into(),
                    groups: vec![1, 1, 3],
                },
                1,
            ),
            (
                Row {
                    springs: ".??..??...?##.".into(),
                    groups: vec![1, 1, 3],
                },
                4,
            ),
            (
                Row {
                    springs: "?#?#?#?#?#?#?#?".into(),
                    groups: vec![1, 3, 1, 6],
                },
                1,
            ),
        ];

        for (input, expected) in tests.iter() {
            let result = find_arrangements(input);
            if result != *expected {
                panic!("Incorrect number of arrangements. got={result}, expected={expected}");
            }
        }
    }
}
