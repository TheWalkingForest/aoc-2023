pub fn process(input: &str) -> i32 {
    todo!("day 12 - part 1");
}

mod part1_tests {
    use super::process;

    #[test]
    fn test_1() {
        let input = "???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1";
        let result = process(input);
        assert_eq!(result, 21);
    }
}
