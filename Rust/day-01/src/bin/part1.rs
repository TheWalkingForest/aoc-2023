use rayon::prelude::*;

fn process(input: &str) -> i32 {
    input
        .par_lines()
        .map(|l| {
            let d = l.chars().filter(|c| c.is_ascii_digit());
            let first: i32 = d.clone().take(1).collect::<String>().parse().unwrap();
            let last: i32 = d.rev().take(1).collect::<String>().parse().unwrap();
            first * 10 + last
        })
        .sum::<i32>()
}

fn main() {
    let input = include_str!("input.txt");
    let result = process(input);
    println!("{result}")
}

#[cfg(test)]
mod proc_tests {
    #[test]
    fn test_1() {
        use super::*;
        let input = "1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet";
        let result = process(input);
        assert_eq!(result, 142)
    }
}
