use rayon::prelude::*;

fn num_to_digit(input: &str) -> String {
    match input {
        "one" => "1".into(),
        "three" => "3".into(),
        "eight" => "8".into(),
        "two" => "2".into(),
        "four" => "4".into(),
        "five" => "5".into(),
        "six" => "6".into(),
        "seven" => "7".into(),
        "nine" => "9".into(),
        _ => "".into(),
    }
}

fn is_number(input: &str) -> bool {
    match input {
        "one" | "three" | "eight" | "two" | "four" | "five" | "six" | "seven" | "nine" => true,
        _ => false,
    }
}

fn parse_num(input: &str) -> (String, usize) {
    let mut i = 0usize;
    let mut buff = String::new();
    while i < input.len() {
        buff.push(input.as_bytes()[i] as char);
        if is_number(&buff) {
            return (num_to_digit(&buff), i);
        }
        i += 1;
    }
    ("".into(), 0)
}

fn parse_line(input: &str) -> String {
    let mut i = 0usize;
    let mut buff = String::new();

    while i < input.len() {
        match input.as_bytes()[i] as char {
            'o' | 't' | 'e' | 'f' | 's' | 'n' => {
                let (s, l) = parse_num(&input[i..]);
                i += l;
                if l == 0 {
                    buff.push(input.as_bytes()[i] as char);
                } else {
                    buff = format!("{buff}{s}");
                }
            }
            _ => {
                buff.push(input.as_bytes()[i] as char);
            }
        }
        i += 1;
    }
    buff
}

fn process(input: &str) -> i32 {
    input
        .par_lines()
        .map(|l| parse_line(l))
        .map(|l| {
            let d = l.chars().filter(|c| c.is_ascii_digit());
            let first: i32 = d.clone().take(1).collect::<String>().parse().unwrap();
            let last: i32 = d.rev().take(1).collect::<String>().parse().unwrap();
            first * 10 + last
        })
        .sum()
}

fn main() {
    let input = include_str!("input.txt");
    let result = process(input);
    println!("{result}");
}

#[cfg(test)]
mod proc_tests {
    use super::*;
    #[test]
    fn test_1() {
        let input = "two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen";
        let result = process(input);
        assert_eq!(result, 281)
    }

    #[test]
    fn test_parse_num() {
        let tests = [
            ("ona", ("".into(), 0usize)),
            ("one", ("1".into(), 2usize)),
            ("two", ("2".into(), 2usize)),
            ("six", ("6".into(), 2usize)),
            ("four", ("4".into(), 3usize)),
            ("five", ("5".into(), 3usize)),
            ("nine", ("9".into(), 3usize)),
            ("three", ("3".into(), 4usize)),
            ("seven", ("7".into(), 4usize)),
            ("eight", ("8".into(), 4usize)),
        ];
        for (input, expected) in tests {
            assert_eq!(parse_num(input), expected);
        }
    }

    #[test]
    fn test_parse_line() {
        let tests = [
            ("one", "1"),
            ("oneight", "1ight"),
            ("oneighttwo", "1ight2"),
            (
                "cbjpqhnrfivefourfxxbctjkhkonebpqlzbdn73",
                "cbjpqhnr54fxxbctjkhk1bpqlzbdn73",
            ),
            (
                "nine9two7xkjdlrrpgxbcfmpfmzsevenkkzxnxbfour",
                "9927xkjdlrrpgxbcfmpfmz7kkzxnxb4",
            ),
            ("5pfzht", "5pfzht"),
            ("fourkv4", "4kv4"),
            ("onebhtglzjsmhncmkfln1xj7", "1bhtglzjsmhncmkfln1xj7"),
            ("oneightwo", "1igh2"),
            ("eightwothree", "8wo3"),
            ("abcone2threexyz", "abc123xyz"),
            ("xtwone3four", "x2ne34"),
            ("4nineeightseven2", "49872"),
            ("zoneight234", "z1ight234"),
            ("7pqrstsixteen", "7pqrst6teen"),
        ];
        for (input, expected) in tests {
            let output = parse_line(input);
            if output != expected.to_string() {
                eprintln!("Incorrect output. got={output}, expected={expected}");
                assert_eq!(output, expected.to_string());
            }
        }
    }
}
